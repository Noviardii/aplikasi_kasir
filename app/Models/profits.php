<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
class profit extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable =[
        'transaction_id','total'
    ];

    /**
     * 
     * @return void
     */
    public function transactions()
    {
        return $this->BelongTo(transactions::class);
    }
}

